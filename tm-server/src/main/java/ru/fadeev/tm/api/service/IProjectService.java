package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    String findIdByName(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    List<Project> findAll(@Nullable String userId) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @NotNull
    Collection<Project> sortByStartDate(@Nullable  String userId) throws Exception;

    @NotNull
    Collection<Project> sortByFinishDate(@Nullable final String userId) throws Exception;

    @NotNull
    Collection<Project> sortByStatus(@Nullable final String userId) throws Exception;

    @NotNull
    Collection<Project> sortByCreationDate(@Nullable final String userId) throws Exception;

    @NotNull
    Collection<Project> searchByName(@Nullable String userId, @Nullable String string) throws Exception;

    @NotNull
    Collection<Project> searchByDescription(@Nullable String userId, @Nullable String string) throws Exception;

    @Nullable
    Project findOne(@Nullable String userId, @Nullable String id) throws Exception;

    void remove(@Nullable String userId,@Nullable String projectId) throws Exception;

}