package ru.fadeev.tm.entity;

import com.fasterxml.jackson.annotation.JsonFilter;
import org.hibernate.annotations.Cache;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Entity(name = "app_session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Session extends AbstractEntity {

    @Nullable
    @ManyToOne
    @JsonFilter("idFilter")
    private User user;

    @Nullable
    private String signature;


    @Column(name = "creation_time")
    private long creationTime = new Date().getTime();

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Role role;

}