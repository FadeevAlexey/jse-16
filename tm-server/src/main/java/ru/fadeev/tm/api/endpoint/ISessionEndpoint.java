package ru.fadeev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    @Nullable
    @WebMethod
    String getToken(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password) throws Exception;

    @WebMethod
    void closeSession(@WebParam(name = "token") @Nullable String token) throws Exception;

}