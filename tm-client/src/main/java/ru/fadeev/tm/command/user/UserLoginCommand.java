package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.IUserEndpoint;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.InvalidSessionException;

public final class UserLoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "log in task manager";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final IUserEndpoint userService = serviceLocator.getUserEndpoint();
        terminal.println("[USER LOGIN]");
        terminal.println("ENTER LOGIN");
        @Nullable final String login = terminal.readString();
        @NotNull final boolean loginExist = userService.isLoginExistUser(login);
        if (!loginExist) throw new IllegalArgumentException("Can't find user");
        terminal.println("ENTER PASSWORD");
        @Nullable final String password = terminal.readString();
        if (password == null) throw new IllegalArgumentException("Illegal password");
        String session = serviceLocator.getSessionEndpoint().getToken(login, password);
        if (session == null || session.isEmpty()) throw new InvalidSessionException("Invalid session");
        serviceLocator.getAppStateService().setToken(session);
        terminal.println("[OK]\n");
    }

}