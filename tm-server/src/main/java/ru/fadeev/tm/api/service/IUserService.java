package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.User;

public interface IUserService extends IService<User> {

    boolean isLoginExist(@Nullable String login) throws Exception;

    @Nullable
    User findUserByLogin(@Nullable String login) throws Exception;

    void setAdminRole (@Nullable User user) throws Exception;

}