###### [https://gitlab.com/FadeevAlexey/jse-16](https://gitlab.com/FadeevAlexey/jse-16)
# Task Manager 1.0.16

A simple console task manager, can help you organize your tasks.

### Built with
  - Java 8
  - Maven 4.0

### Developer
Alexey Fadeev
[alexey.v.fadeev@gmail.com](mailto:alexey.v.fadeev@gmail.com?subject=TaskManager)

### Building from source

```sh
$ git clone http://gitlab.volnenko.school/FadeevAlexey/jse-16.git
$ cd jse-16
$ mvn clean
$ mvn install
```

### Server running

```sh
$ java -jar tm-server/target/release/bin/tm-server.jar
```

### Client running

```sh
$ java -jar tm-client/target/release/bin/tm-client.jar
```

