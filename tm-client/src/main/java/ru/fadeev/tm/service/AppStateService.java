package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.IAppStateRepository;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;

import java.util.*;

public class AppStateService  implements IAppStateService {

    private final IAppStateRepository appStateRepository;

    public AppStateService(IAppStateRepository appStateRepository) {
        this.appStateRepository = appStateRepository;
    }

    @Override
    public void putCommand(@Nullable final String description, @Nullable final AbstractCommand abstractCommand) {
        if (description == null || description.isEmpty()) return;
        if (abstractCommand == null) return;
        appStateRepository.putCommand(description, abstractCommand);
    }

    @Override
    @Nullable
    public AbstractCommand getCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return null;
        return appStateRepository.getCommand(command);
    }

    @Override
    @NotNull
    public List<AbstractCommand> getCommands() {
        return appStateRepository.getCommands();
    }

    @Override
    public @Nullable String getToken() {
        return appStateRepository.getToken();
    }

    @Override
    public void setToken(@Nullable String token) {
        appStateRepository.setToken(token);
    }

}