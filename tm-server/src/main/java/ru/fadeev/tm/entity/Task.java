package ru.fadeev.tm.entity;

import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Entity(name = "app_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Task extends AbstractEntity {

    @Nullable
    @ManyToOne
    private Project project;

    @Nullable
    @ManyToOne
    private User user;

    @Nullable
    private String name = "";

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String description = "";

    @Nullable
    @Column(name = "start_date")
    private Date startDate;

    @Nullable
    @Column(name = "finish_date")
    private Date finishDate;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Status status = Status.PLANNED;

    @Nullable
    @Column(name = "creation_time")
    private Date creationTime = new Date(System.currentTimeMillis());

}