package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;

public class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getAppStateService().getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        serviceLocator.getProjectEndpoint().removeAllProject(token);
        serviceLocator.getTaskEndpoint().removeAllProjectTask(token);
        serviceLocator.getTerminalService().println("[ALL PROJECTS REMOVE]\n");
    }

}