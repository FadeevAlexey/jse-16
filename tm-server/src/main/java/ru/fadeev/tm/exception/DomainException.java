package ru.fadeev.tm.exception;

import org.jetbrains.annotations.Nullable;

public final class DomainException extends RuntimeException {

    public DomainException(@Nullable final String message) {
        super(message);
    }

}
